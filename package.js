Package.describe({
    summary: "Vazco CMS core package."
});

Package.on_use(function (api) {
    api.use([
        'vazco-tools-common'
    ], [
        'client',
        'server'
    ]);

    api.add_files([
        'core.js'
    ], [
        'client',
        'server'
    ]);
});