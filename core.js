'use strict';

// Define prototypes and inheritance.

// Entity
Vazco.Entity = function VazcoEntity() {
};

// Object
Vazco.Object = function VazcoObject(doc) {
    _.extend(this, doc);
};

Vazco.Object.prototype = Vazco.Entity.prototype;

// Group
Vazco.Group = function VazcoGroup(doc) {
    _.extend(this, doc);
};

Vazco.Group.prototype.prototype = Vazco.Entity.prototype;

// User
Vazco.User = function VazcoUser(doc) {
    _.extend(this, doc);
};

Vazco.User.prototype.prototype = Vazco.Entity.prototype;


// Define schemas and methods.

Vazco.Entity.prototype.schema = {
    create_ts: {
        type: Date,
        autoValue: function () {
            if (this.isInsert || this.isUpsert) {
                return new Date();
            }
        },
        denyUpdate: true
    },
    edit_ts: {
        type: Date,
        optional: true,
        autoValue: function () {
            if (this.isUpdate) {
                return new Date();
            }
        }
    },
    relations: {
        type: Object,
        optional: true,
        blackbox: true
    }
};

